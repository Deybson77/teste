const express = require('express');
const app = express();
var port = process.env.PORT||3000;


var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

 
var Usuario = require('./api/models/Usuario')
var rotas = require('./api/routes/routes');
rotas(app);

app.listen(port);
console.log("Servidor rodando no endereco 127.0.0.1:3000 com sucesso")
