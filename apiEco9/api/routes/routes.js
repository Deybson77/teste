module.exports = function(app){
	var usuarioController = require('../controllers/controllerUsuario');

	app.route('/usuario')
		.get(usuarioController.listarUsuarios)
		.post(usuarioController.cadastrarUsuario);
}
