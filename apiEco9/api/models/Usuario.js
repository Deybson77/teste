const mongoose = require('../dtbase/conexao');
var Schema = mongoose.Schema;

var UsuarioSchema = new Schema({
	nome:{
		type: String,
		required: true,
	},
	sexo:{
		type: String
	},
	dataNascimento:{
		type: Date,
		default: Date.now
	},
	endereco:{
		type: [{
			rua: String,
			nº: String,
			logradouro: String,
			bairro: String,
			cidade: String,
			estado: String

		}]
	},
	email:{
		type: String,
		required: true,
		unique: true,
		lowercase: true

	},
	telefone:{
		type: [{
			DDD: Number,
			numero: Number
		}]
	},
	senha:{
		type:String,
		required: true,
	}

});
module.exports = mongoose.model('Usuario', UsuarioSchema);